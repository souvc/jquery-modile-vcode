#jquery-modile-vcode.js  是一款用于前端开发的时候，获取验证码的一个60秒等待体验的简单js插件。


##第一步：添加html文件

```
<input type="text" class="lr-input" placeholder="请输入短信验证码" id="code" name="code" value="">
<input id="btnSendCode1" class="lr-btnSendCode" value="获取验证码" type="button">

```


##第二步：添加js文件


```
<script src="jquery-1.9.1.min.js"></script>
<script src="jquery-modile-vcode.js"></script>

```


##第三步：启用插件。


```
<script type="text/javascript">
/*
*******************************
*  手机验证码
*******************************
*/
$(".lr-btnSendCode").modileCode({
	inputVerily: true,
     verilyFunction : function(obj){  //表单验证函数，必须配置inputVerily为true
       /*  var flag;
        flag = $("#register_modile").val() != ""&&$("#register_modile").val() != $("#register_modile").attr("placeholder");
        if(!flag){
            alert("请输入手机号");
        } */
        //return flag;
    	 return true;
    },
	count : 300
},function(){
	//向后台发送处理数据
	//$.ajax({
	//   type: "POST", //用POST方式传输
	//   dataType: "text", //数据格式:JSON
	//   url: '', //目标地址-后台
	//   data: "dealType=" + dealType +"&uid=" + uid + "&code=" + code,
	//   error: function (XMLHttpRequest, textStatus, errorThrown) { },
	//   success: function (msg){ }
	//});
});
</script>

```



##第三步：运行index.html 文件。


##第四步：查看效果。http://souvc.oschina.io/jquery-modile-vcode/
